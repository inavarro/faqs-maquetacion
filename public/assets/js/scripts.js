const mainBody = document.querySelector(".faqs-maquetacion");
const mainHeader = document.querySelector(".main-header");
const mainSidebar = document.querySelector(".main-sidebar");
const metaThemeColor = document.querySelector("meta[name=theme-color]");
const hamburguer = document.querySelector(".main-header__hamburguer");

function toggle() {
	mainBody.classList.toggle("dark");
	if (mainBody.classList.contains("dark")) {
		metaThemeColor.setAttribute("content", "#232323");
	    localStorage.removeItem("theme");
	    localStorage.setItem("theme", "dark");
	} else {
		metaThemeColor.setAttribute("content", "#FFFFFF");
		localStorage.removeItem("theme");
	    localStorage.setItem("theme", "light");
	}
}

function sidebar() {
	if (!mainBody.classList.contains("sidebar-displayed")) {
		localStorage.removeItem("sidebar-preferance");
	    localStorage.setItem("sidebar-preferance", "sidebar-displayed");
	} else {
		localStorage.removeItem("sidebar-preferance");
		localStorage.setItem("sidebar-preferance", "sidebar-hidden");
	}
	mainSidebar.classList.toggle("sidebar-displayed");
	mainBody.classList.toggle("sidebar-displayed");
	hamburguer.classList.toggle("sidebar-displayed");
}

window.onscroll = function() {
	if (window.pageYOffset > 0) {
		mainHeader.classList.add("shrink");
		mainBody.classList.add("header-shrink");
	}
}

window.onload = function() {
	if (window.matchMedia("(min-width: 768px)").matches) {
		mainSidebar.classList.add("sidebar-displayed");
		mainBody.classList.add("sidebar-displayed");
		hamburguer.classList.add("sidebar-displayed");
	}
	if (typeof(Storage) !== "undefined") {
		mainBody.classList.add(localStorage.getItem("theme"));
	    if (localStorage.getItem("theme") == "dark") {
	    	metaThemeColor.setAttribute("content", "#232323");
	    }
		if (localStorage.getItem("sidebar-preferance") == "sidebar-hidden") {
			mainSidebar.classList.remove("sidebar-displayed");
			mainBody.classList.remove("sidebar-displayed");
			hamburguer.classList.remove("sidebar-displayed");
		}
	}
}